/**
 * Main script to insert data inside mongo
 */
const { Pool } = require('multiprocessing')
const es = require('event-stream')
const fs = require('fs')
const path = require('path')

const numberProcess = 1
const fileSource = './raw-data/StockEtablissement_utf8.csv'
const slicedDir = './processed-data'
const fileSizeInBytes = parseInt(28175312 / 100, 10)

/**
 * Map file's name array into a Processes array and start multiprocessing
 *
 * @param {array} list list of files inside the splited directory
 * @returns {number} 1 if the operation was a success
 */
const multiProcess = (list) => {
  const pool = new Pool(numberProcess)
  pool.map(list, (p) => {
    const Process = require(`${__dirname}/../../../process.js`)
    const process = new Process(p)
    process.processFile()
    return 1
  })
  return 1
}

/**
 * Splice the original document in several parts who will be used in multiprocessing
 *
 * @param {file} file the big file we need to split
 * @param {number} fileSize number of maximum file size in bytes
 * @returns {string} state of the split attempt
 */
const spliceAndDice = (file, fileSize) => {
  let lineCount = 0
  let countInFile = 0
  let part = 0
  let fileSliced = []
  fs.createReadStream(file)
    .pipe(es.split())
    .pipe(
      es
        .mapSync((line) => {
          lineCount += 1
          countInFile += 1
          console.log(line)
          // ADD IN FILE
          if (!(part === 0 && line === 1)) {
            fileSliced.push(`${line}\n`)
          }
          if (fileSize === countInFile && lineCount > 1) {
            countInFile = 0
            // save file
            fs.writeFile(`${slicedDir}/part${part}.csv`, fileSliced, (err) => {
              if (err) {
                return err
              }
              part += 1
              fileSliced = []
              return `The file ${part} was saved!`
            })
          }
        })
        .on('error', (err) => {
          console.log(err)
        })
        .on('end', () => {
          fs.writeFile(`${slicedDir}/part${part}.csv`, fileSliced, (err) => {
            if (err) {
              return err
            }
            part += 1
            fileSliced = []
            return `The file ${part} was saved!`
          })
          console.log(lineCount)
        })
    )
}

/**
 * Check if a string contain a certain regex and remove that regex
 *
 * @param {array} originalArray Array of string
 * @param {regex} regex The regex we need to check
 * @returns {array} clean array without regex
 */
const removeMatching = (originalArray, regex) => {
  let j = 0
  while (j < originalArray.length) {
    if (regex.test(originalArray[j])) originalArray.splice(j, 1)
    else j += 1
  }
  return originalArray
}

/**
 * Function who check files inside the data directory and start multiprocessing
 *
 * @param {string} directoryPath Path to the directory we want to check
 * @returns succes or fail depending of the multiprocess function
 */
const getFilesList = directoryPath => fs.readdir(directoryPath, (err, files) => {
  // handling error
  if (err) {
    return `Unable to scan directory: ${err}`
  }
  const regExpStuff = /-done/
  if (process.argv[2] === '--no-cache' || process.argv[2] === '-n') {
    console.log(files)

    // clean to directory because of the no cache
    const directory = 'processed-data'
    fs.readdir(directory, (erCl, filesToClean) => {
      if (erCl) throw err
      filesToClean.forEach((file) => {
        fs.unlink(path.join(directory, file), (error) => {
          if (error) throw error
        })
      })
    })

    return multiProcess(files)
  }
  const array = removeMatching(files, regExpStuff)
  console.log(array)
  return multiProcess(array)
})

/**
 * We check if there is an option when running the script and if there is
 * decide if we need to split and override files or not
 */
if (process.argv[2] === '--no-cache' || process.argv[2] === '-n') {
  spliceAndDice(fileSource, fileSizeInBytes, slicedDir)
}
getFilesList(slicedDir)
