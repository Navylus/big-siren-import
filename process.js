const { MongoClient } = require('mongodb')
const es = require('event-stream')
const fs = require('fs')
const Sirene = require('./models/sirene-model')

/**
 * Class used to process sliced files
 * This class is used inside children processes
 */
module.exports = class Process {
  constructor(file) {
    this.file = file
  }

  /**
   * Function who split a line and return a Sirene object
   *
   * @param {number} line Line to format
   * @returns {Sirene} object that we want to insert insede mongo
   */
  formatLine(line) {
    const partsOfStr = line.split(',')
    return new Sirene({
      siren: partsOfStr[0],
      nic: partsOfStr[1],
      siret: partsOfStr[2],
      statutdiffusionetablissement: partsOfStr[3],
      datecreationetablissement: partsOfStr[4],
      trancheeffectifsetablissement: partsOfStr[5],
      anneeeffectifsetablissement: partsOfStr[6],
      activiteprincipaleregistremetiersetablissement: partsOfStr[7],
      to_char: partsOfStr[7],
      etablissementsiege: partsOfStr[8],
      nombreperiodesetablissement: partsOfStr[9],
      complementadresseetablissement: partsOfStr[10],
      numerovoieetablissement: partsOfStr[11],
      indicerepetitionetablissement: partsOfStr[12],
      typevoieetablissement: partsOfStr[13],
      libellevoieetablissement: partsOfStr[14],
      codepostaletablissement: partsOfStr[15],
      libellecommuneetablissement: partsOfStr[16],
      libellecommuneetrangeretablissement: partsOfStr[17],
      distributionspecialeetablissement: partsOfStr[18],
      codecommuneetablissement: partsOfStr[19],
      codecedexetablissement: partsOfStr[20],
      libellecedexetablissement: partsOfStr[21],
      codepaysetrangeretablissement: partsOfStr[22],
      libellepaysetrangeretablissement: partsOfStr[23],
      complementadresse2etablissement: partsOfStr[24],
      numerovoie2etablissement: partsOfStr[25],
      indicerepetition2etablissement: partsOfStr[26],
      typevoie2etablissement: partsOfStr[27],
      libellevoie2etablissement: partsOfStr[28],
      codepostal2etablissement: partsOfStr[29],
      libellecommune2etablissement: partsOfStr[30],
      libellecommuneetranger2etablissement: partsOfStr[31],
      distributionspeciale2etablissement: partsOfStr[32],
      codecommune2etablissement: partsOfStr[33],
      codecedex2etablissement: partsOfStr[34],
      libellecedex2etablissement: partsOfStr[35],
      codepaysetranger2etablissement: partsOfStr[36],
      libellepaysetranger2etablissement: partsOfStr[37],
      datedebut: partsOfStr[37],
      etatadministratifetablissement: partsOfStr[38],
      enseigne1etablissement: partsOfStr[39],
      enseigne2etablissement: partsOfStr[40],
      enseigne3etablissement: partsOfStr[41],
      denominationusuelleetablissement: partsOfStr[42],
      activiteprincipaleetablissement: partsOfStr[43],
      nomenclatureactiviteprincipaleetablissement: partsOfStr[44],
      caractereemployeuretablissement: partsOfStr[45]
    })
  }

  /**
   * Function who take a file split it using formatLine(line)
   * insert the given object into a bulk and then insert it in mongoDB
   * and then rename the file who was inserted in order to "save" the state
   */
  async processFile() {
    const fileName = this.file
    const client = await MongoClient.connect('mongodb://localhost/')
    const end = new Promise((resolve) => {
      const db = client.db('BigSirene')
      // Get then collection
      const col = db.collection('bigDB')
      let batch = col.initializeOrderedBulkOp()
      let lineCount = 0
      fs.createReadStream(`./processed-data/${fileName}`)
        .pipe(es.split())
        .pipe(
          es
            .mapSync((line) => {
              const remainder = lineCount % 1000
              if (remainder === 0) {
                batch.execute((err, result) => {
                  if (err) return err
                  batch = col.initializeOrderedBulkOp()
                  return result
                })
              }
              const newSir = this.formatLine(line)
              console.log(newSir)
              batch.insert(newSir)
              lineCount += 1
            })
            .on('end', () => {
              db.close()
              fs.rename(
                `./processed-data/${fileName}`,
                `./processed-data/${fileName}-done.csv`,
                (err) => {
                  if (err) throw err
                  console.log('renamed complete')
                }
              )
              resolve(`${lineCount} inserted from this file`)
            })
        )
    })

    const insertFile = async () => {
      const inserted = await end
      console.log(inserted)
    }
    insertFile()
  }
}
