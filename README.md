This project is here to insert lot of data inside MongoDB

Clone this repo and then create 2 folders :

- raw-data
- processed-data

Who then need to install packages ( **npm i** )

Place the files containing Sirene data called **Sirene : Fichier StockEtablissement** inside raw-data folder and rename it **StockEtablissement_utf8.csv** (
[data-gouv-siren-siret](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/))

you need to have a mongodb process listening on the standart port in our case the collection is named BigSirene

you can run the script whith no option or with **-n / --no-cache** this option will make the script split the data again and ignore already inserted data

run the script with:
**node index.js (-n/--no-cache)**
